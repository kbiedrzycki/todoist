module.exports = async function (browser, inputParameters) {
  if (!inputParameters.testTargetUrl) {
    throw 'Invalid event data!';
  }

  await browser.get(inputParameters.testTargetUrl);
  const title = await browser.getTitle();

  return { pageTitle: title };
};

